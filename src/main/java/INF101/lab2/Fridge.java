package INF101.lab2;
import java.util.ArrayList;
//import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private List<FridgeItem> items = new ArrayList<>();
    private int maxSize = 10;

    /**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
	public int nItemsInFridge() {
        return this.items.size();
    }

	/**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
	public int totalSize() {
        return this.maxSize;
    }

	/**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
	public boolean placeIn(FridgeItem item) {
        if (this.nItemsInFridge() == this.totalSize()) {
            return false;
        } else {
            return this.items.add(item);
        } 
    }

	/**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
	public void takeOut(FridgeItem item) {
		if (this.nItemsInFridge() == 0) {
			NoSuchElementException noSuchElement = new NoSuchElementException();
			throw noSuchElement;
		} else {
	        this.items.remove(item);
		}
    }

	/**
	 * Remove all items from the fridge
	 */
	public void emptyFridge() {
        this.items.clear();
    }

	/**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
	public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem thisItem : this.items) {
            if (thisItem.hasExpired()) {
                expiredFood.add(thisItem);
            }
        }
		for (FridgeItem thisItem : expiredFood) {
            this.items.remove(thisItem);
        }
        return expiredFood;
    }
}
